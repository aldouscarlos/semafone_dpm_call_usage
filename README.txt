##  Below are the details of how the shell script and the log file were deployed, permissions and location, including crontab entry.  THESE ARE DEPLOYED IN DPMx ONLY.
##
##
sudo touch /var/log/semarep.txt
sudo chown root.adm /var/log/semarep.txt
sudo chmod 666 /var/log/semarep.txt
sudo touch /usr/local/bin/runsearch.sh
sudo chown root.adm /usr/local/bin/runsearch.sh
sudo chmod 755 /usr/local/bin/runsearch.sh
sudo vi /usr/local/bin/runsearch.sh
##
##
##
##

sysadmin@syd2sema001sedb:~$ cat /usr/local/bin/runsearch.sh
#!/bin/bash
#
#
#VARIABLES
#
filedir=/var/log/semafone/
filenam="catalina.out."`date -d yesterday +%F`".gz"
logfil=/var/log/semarep.txt
totcalls=0
totsecure=0
totccsecure=0
svrnam=`hostname`
#
#
#FUNCTIONS
#
logfileclear()  {
cat /dev/null > $logfil

return 0
}
#
#
totalcallcount() {
totcalls=`zcat $filedir$filenam |egrep -c "Call Established"`

return 0
}
#
#
totalsecuremodecount() {
totsecure=`zcat $filedir$filenam |egrep -c "Enter Secure Mode"`

return 0
}
#
#
totalccsecurecount() {
totccsecure=`zcat $filedir$filenam |egrep -c "Auto Return To Non Secure Mode From Secure CR Mode"`

return 0
}
#
#
filerep() {
echo ${svrnam^^}: TotalCallsMade=$totcalls : TotalCallsInSecureMode=$totsecure : TotalCallsWithCardCapture=$totccsecure>$logfil
logger -f $logfil

return 0
}
#
#
#
#MAIN
#
logfileclear
totalcallcount
totalsecuremodecount
totalccsecurecount
filerep
#
#
#
exit 0



##
##
##
##
sysadmin@syd1sema001dpma:~$ ls -ltr /usr/local/bin/runsearch.sh
-rwxr-xr-x 1 root adm 879 Sep 16 13:29 /usr/local/bin/runsearch.sh
##
##
sysadmin@syd1sema001dpma:~$ ls -ltr /var/log/semarep.txt
-rw-rw-rw- 1 root adm 92 Sep 20 06:01 /var/log/semarep.txt
##
##
##
# m h  dom mon dow   command
01 06 * * * /usr/local/bin/runsearch.sh > /dev/null 2>&1